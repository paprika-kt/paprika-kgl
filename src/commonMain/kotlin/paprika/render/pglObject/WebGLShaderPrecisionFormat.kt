package paprika.render.pglObject

interface WebGLShaderPrecisionFormat {
    val rangeMin: Int
    val rangeMax: Int
    val precision: Int
}