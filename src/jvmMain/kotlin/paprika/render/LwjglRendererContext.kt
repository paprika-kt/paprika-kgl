package paprika.render

import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL13.glActiveTexture
import org.lwjgl.opengl.GL15C
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30.*
import org.lwjgl.opengl.GL45
import org.lwjgl.system.MemoryUtil.memAddress
import paprika.canvas.sources.ImageSource
import paprika.io.buffer.ByteArrayBuffer
import paprika.io.buffer.FloatArrayBuffer
import paprika.io.buffer.IntArrayBuffer
import paprika.io.buffer.ShortArrayBuffer
import paprika.render.pglObject.*

class LwjglRendererContext : RenderingContext {

    override val drawingBufferWidth: Int
        get() = TODO("not implemented")
    override val drawingBufferHeight: Int
        get() = TODO("not implemented")

    override fun getContextAttributes(): PGLContextAttributes? {
        TODO("not implemented")
    }

    override fun isContextLost(): Boolean {
        TODO("not implemented")
    }

    override fun getSupportedExtensions(): Array<String> {
        TODO("not implemented")
    }

    override fun getExtension(name: String): Any {
        TODO("not implemented")
    }

    override fun activeTexture(texture: Int) = glActiveTexture(texture)

    override fun attachShader(program: PGLProgram, shader: PGLShader) = glAttachShader(program.id, shader.id)

    override fun bindAttribLocation(program: PGLProgram, index: Int, name: String) = glBindAttribLocation(program.id, index, name)

    override fun bindBuffer(target: Int, buffer: PGLBuffer?) = glBindBuffer(target, buffer?.id ?: 0)

    override fun bindFramebuffer(target: Int, framebuffer: WebGLFramebuffer) = glBindFramebuffer(target, framebuffer.id)

    override fun bindRenderbuffer(target: Int, renderbuffer: PGLRenderbuffer) = glBindRenderbuffer(target, renderbuffer.id)

    override fun bindTexture(target: Int, texture: PGLTexture) = glBindTexture(target, texture.id)

    override fun blendColor(red: Float, green: Float, blue: Float, alpha: Float) = glBlendColor(red, green, blue, alpha)

    override fun blendEquation(mode: Int) = glBlendEquation(mode)

    override fun blendEquationSeparate(modeRGB: Int, modeAlpha: Int) = glBlendEquationSeparate(modeRGB, modeAlpha)

    override fun blendFunc(sfactor: Int, dfactor: Int) = glBlendFunc(sfactor, dfactor)

    override fun blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int) = glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha)

    override fun bufferData(target: Int, size: Int, usage: Int) = glBufferData(target, size.toLong(), usage)

    override fun bufferData(target: Int, data: PBufferDataSource?, usage: Int) {
        TODO("not implemented")
    }

    // TODO to review
    override fun bufferData(target: Int, data: FloatArrayBuffer, usage: Int) = GL15C.nglBufferData(target, data.byteLength.toLong(), memAddress(data.buffer), usage) //glBufferData(target, data.buffer, usage)

    override fun bufferData(target: Int, data: ShortArrayBuffer, usage: Int) = GL15C.nglBufferData(target, data.byteLength.toLong(), memAddress(data.buffer), usage) //glBufferData(target, data.buffer, usage)
    override fun bufferData(target: Int, data: IntArrayBuffer, usage: Int) = GL15C.nglBufferData(target, data.byteLength.toLong(), memAddress(data.buffer), usage) //glBufferData(target, data.buffer, usage)
    override fun bufferData(target: Int, data: ByteArrayBuffer, usage: Int) = GL15C.nglBufferData(target, data.byteLength.toLong(), memAddress(data.buffer), usage) //glBufferData(target, data.buffer, usage)

    override fun bufferSubData(target: Int, offset: Int, data: PBufferDataSource?) {
        TODO("not implemented")
    }

    override fun checkFramebufferStatus(target: Int): Int {
        TODO("not implemented")
    }

    override fun clear(mask: Int) = glClear(mask)
    override fun clearColor(red: Float, green: Float, blue: Float, alpha: Float) = glClearColor(red, green, blue, alpha)
    override fun clearDepth(depth: Float) = glClearDepth(depth.toDouble())
    override fun clearStencil(s: Int) = glClearStencil(s)
    override fun colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean) = glColorMask(red, green, blue, alpha)
    override fun compileShader(shader: PGLShader) = glCompileShader(shader.id)

    override fun compressedTexImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, data: PGLArrayBufferView) {
        TODO("not implemented")
    }

    override fun compressedTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, data: PGLArrayBufferView) {
        TODO("not implemented")
    }

    override fun copyTexImage2D(target: Int, level: Int, internalformat: Int, x: Int, y: Int, width: Int, height: Int, border: Int) {
        TODO("not implemented")
    }

    override fun copyTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, x: Int, y: Int, width: Int, height: Int) {
        TODO("not implemented")
    }

    override fun createBuffer(): PGLBuffer = LwjglBuffer(glGenBuffers())

    override fun createFramebuffer(): WebGLFramebuffer? {
        TODO("not implemented")
    }

    override fun createProgram(): PGLProgram? = LwjglProgram(glCreateProgram())

    override fun createRenderbuffer(): PGLRenderbuffer? = LwjglRenderBuffer(GL45.glCreateRenderbuffers())

    override fun createShader(type: Int): PGLShader = LwjglShader(glCreateShader(type))

    override fun createTexture(): PGLTexture = LwjglTexture(glGenTextures())

    override fun cullFace(mode: Int) = glCullFace(mode)

    override fun deleteBuffer(buffer: PGLBuffer) = glDeleteBuffers(buffer.id)

    override fun deleteFramebuffer(framebuffer: WebGLFramebuffer) = glDeleteFramebuffers(framebuffer.id)

    override fun deleteProgram(program: PGLProgram) = glDeleteProgram(program.id)

    override fun deleteRenderbuffer(renderbuffer: PGLRenderbuffer) = glDeleteRenderbuffers(renderbuffer.id)

    override fun deleteShader(shader: PGLShader) = glDeleteShader(shader.id)

    override fun deleteTexture(texture: PGLTexture) = glDeleteTextures(texture.id)

    override fun depthFunc(func: Int) = glDepthFunc(func)

    override fun depthMask(flag: Boolean) = glDepthMask(flag)

    override fun depthRange(zNear: Float, zFar: Float) = glDepthRange(zNear.toDouble(), zFar.toDouble())

    override fun detachShader(program: PGLProgram, shader: PGLShader) = glDetachShader(program.id, shader.id)

    override fun disable(cap: Int) = glDisable(cap)

    override fun disableVertexAttribArray(index: Int) = glDisableVertexAttribArray(index)

    override fun drawArrays(mode: Int, first: Int, count: Int) = glDrawArrays(mode, first, count)

    override fun drawElements(mode: Int, count: Int, type: Int, offset: Int) = glDrawElements(mode, count, type, offset.toLong())

    override fun enable(cap: Int) = glEnable(cap)

    override fun enableVertexAttribArray(index: Int) = glEnableVertexAttribArray(index)

    override fun finish() = glFinish()

    override fun flush() = glFlush()

    override fun framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: PGLRenderbuffer) = glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer.id)

    override fun framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: PGLTexture, level: Int) = glFramebufferTexture2D(target, attachment, textarget, texture.id, level)

    override fun frontFace(mode: Int) = glFrontFace(mode)

    override fun generateMipmap(target: Int) = glGenerateMipmap(target)

    override fun getActiveAttrib(program: PGLProgram, index: Int): PGLActiveInfo? {
        TODO("not implemented")
    }

    override fun getActiveUniform(program: PGLProgram, index: Int): PGLActiveInfo? {
        TODO("not implemented")
    }

    override fun getAttachedShaders(program: PGLProgram): Array<PGLShader>? {
        TODO("not implemented")
    }

    override fun getAttribLocation(program: PGLProgram, name: String): Int = glGetAttribLocation(program.id, name)

    override fun getBufferParameter(target: Int, pname: Int): Any? = glGetBufferParameteri(target, pname)

    override fun getParameter(pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getError(): Int {
        TODO("not implemented")
    }

    override fun getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getProgramParameter(program: PGLProgram, pname: Int): Any? {
        return glGetProgrami(program.id, pname)
    }

    override fun getProgramInfoLog(program: PGLProgram): String? {
        return glGetProgramInfoLog(program.id)
    }

    override fun getRenderbufferParameter(target: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getShaderParameter(shader: PGLShader, pname: Int): Any? {
        return glGetShaderi(shader.id, pname)
    }

    override fun getShaderPrecisionFormat(shadertype: Int, precisiontype: Int): WebGLShaderPrecisionFormat? {
        TODO("not implemented")
    }

    override fun getShaderInfoLog(shader: PGLShader): String? {
        return GL20.glGetShaderInfoLog(shader.id)
    }

    override fun getShaderSource(shader: PGLShader): String? {
        TODO("not implemented")
    }

    override fun getTexParameter(target: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getUniform(program: PGLProgram, location: PGLUniformLocation): Any? {
        TODO("not implemented")
    }

    override fun getUniformLocation(program: PGLProgram, name: String): PGLUniformLocation = LwjglUniformLocation(glGetUniformLocation(program.id, name))

    override fun getVertexAttrib(index: Int, pname: Int): Any? {
        TODO("not implemented")
    }

    override fun getVertexAttribOffset(index: Int, pname: Int): Int {
        TODO("not implemented")
    }

    override fun hint(target: Int, mode: Int) {
        TODO("not implemented")
    }

    override fun isBuffer(buffer: PGLBuffer): Boolean = glIsBuffer(buffer.id)

    override fun isEnabled(cap: Int): Boolean = glIsEnabled(cap)

    override fun isFramebuffer(framebuffer: WebGLFramebuffer?): Boolean {
        TODO("not implemented")
    }

    override fun isProgram(program: PGLProgram): Boolean = glIsProgram(program.id)

    override fun isRenderbuffer(renderbuffer: PGLRenderbuffer): Boolean = glIsRenderbuffer(renderbuffer.id)

    override fun isShader(shader: PGLShader): Boolean = glIsShader(shader.id)

    override fun isTexture(texture: PGLTexture): Boolean = glIsTexture(texture.id)

    override fun lineWidth(width: Float) = glLineWidth(width)

    override fun linkProgram(program: PGLProgram) = glLinkProgram(program.id)

    override fun pixelStorei(pname: Int, param: Int) = glPixelStorei(pname, param)

    override fun polygonOffset(factor: Float, units: Float) {
        TODO("not implemented")
    }

    override fun readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, type: Int, pixels: PGLArrayBufferView?) {
        TODO("not implemented")
    }

    override fun renderbufferStorage(target: Int, internalformat: Int, width: Int, height: Int) {
        TODO("not implemented")
    }

    override fun sampleCoverage(value: Float, invert: Boolean) {
        TODO("not implemented")
    }

    override fun scissor(x: Int, y: Int, width: Int, height: Int) {
        TODO("not implemented")
    }

    override fun shaderSource(shader: PGLShader, source: String) = glShaderSource(shader.id, source)

    override fun stencilFunc(func: Int, ref: Int, mask: Int) {
        TODO("not implemented")
    }

    override fun stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int) {
        TODO("not implemented")
    }

    override fun stencilMask(mask: Int) {
        TODO("not implemented")
    }

    override fun stencilMaskSeparate(face: Int, mask: Int) {
        TODO("not implemented")
    }

    override fun stencilOp(fail: Int, zfail: Int, zpass: Int) {
        TODO("not implemented")
    }

    override fun stencilOpSeparate(face: Int, fail: Int, zfail: Int, zpass: Int) {
        TODO("not implemented")
    }

    override fun texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, type: Int, pixels: ByteArrayBuffer) = glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels.buffer)
//    override fun texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, type: Int, pixels: UInt8Buffer) = glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels.buffer)

    override fun texImage2D(target: Int, level: Int, internalformat: Int, format: Int, type: Int, source: ImageSource) = glTexImage2D(target, level, internalformat, source.width, source.height, 0, format, type, source.bytes.buffer)

    override fun texParameterf(target: Int, pname: Int, param: Float) = glTexParameterf(target, pname, param)

    override fun texParameteri(target: Int, pname: Int, param: Int) = glTexParameteri(target, pname, param)

    override fun texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, type: Int, pixels: PGLArrayBufferView?) {
        TODO("not implemented")
    }

    override fun texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, type: Int, source: ImageSource?) {
        TODO("not implemented")
    }

    override fun uniform1f(location: PGLUniformLocation, x: Float) {
        TODO("not implemented")
    }

    override fun uniform1fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    //TODO review remove
    override fun uniform1fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform1i(location: PGLUniformLocation, x: Int) = glUniform1i(location.id, x)

    override fun uniform1iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform1iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniform2f(location: PGLUniformLocation, x: Float, y: Float) {
        TODO("not implemented")
    }

    override fun uniform2fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    override fun uniform2fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform2i(location: PGLUniformLocation, x: Int, y: Int) {
        TODO("not implemented")
    }

    override fun uniform2iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform2iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniform3f(location: PGLUniformLocation, x: Float, y: Float, z: Float) {
        TODO("not implemented")
    }

    override fun uniform3fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    override fun uniform3fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform3i(location: PGLUniformLocation, x: Int, y: Int, z: Int) {
        TODO("not implemented")
    }

    override fun uniform3iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform3iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniform4f(location: PGLUniformLocation, x: Float, y: Float, z: Float, w: Float) {
        TODO("not implemented")
    }

    override fun uniform4fv(location: PGLUniformLocation, v: FloatArray) {
        TODO("not implemented")
    }

    override fun uniform4fv(location: PGLUniformLocation, v: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniform4i(location: PGLUniformLocation, x: Int, y: Int, z: Int, w: Int) {
        TODO("not implemented")
    }

    override fun uniform4iv(location: PGLUniformLocation, v: IntArray) {
        TODO("not implemented")
    }

    override fun uniform4iv(location: PGLUniformLocation, v: Array<Int>) {
        TODO("not implemented")
    }

    override fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) {
        TODO("not implemented")
    }

    override fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) {
        TODO("not implemented")
    }

    override fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) {
        TODO("not implemented")
    }

    override fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) = glUniformMatrix4fv(location.id, transpose, value)

    override fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) {
        TODO("not implemented")
    }

    override fun useProgram(program: PGLProgram?) = glUseProgram(program?.id ?: 0)

    override fun validateProgram(program: PGLProgram) {
        TODO("not implemented")
    }

    override fun vertexAttrib1f(index: Int, x: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib1fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttrib2f(index: Int, x: Float, y: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib2fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttrib3f(index: Int, x: Float, y: Float, z: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib3fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttrib4f(index: Int, x: Float, y: Float, z: Float, w: Float) {
        TODO("not implemented")
    }

    override fun vertexAttrib4fv(index: Int, values: FloatArray) {
        TODO("not implemented")
    }

    override fun vertexAttribPointer(index: Int, size: Int, type: Int, normalized: Boolean, stride: Int, offset: Int) = glVertexAttribPointer(index, size, type, normalized, stride, offset.toLong())

    override fun viewport(x: Int, y: Int, width: Int, height: Int) = glViewport(x, y, width, height)
}

fun createCapabilities() {
    GL.createCapabilities()
}