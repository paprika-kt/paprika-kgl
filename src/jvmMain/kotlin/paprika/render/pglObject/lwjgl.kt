package paprika.render.pglObject


class LwjglProgram(override val id: Int) : LwjglObject, PGLProgram
class LwjglShader(override val id: Int) : PGLShader, LwjglObject
class LwjglRenderBuffer(override val id: Int) : PGLRenderbuffer, LwjglObject
class LwjglTexture(override val id: Int) : PGLTexture, LwjglObject
class LwjglUniformLocation(override val id: Int) : PGLUniformLocation, LwjglObject
class LwjglBuffer(override val id: Int) : PGLBuffer, LwjglObject
