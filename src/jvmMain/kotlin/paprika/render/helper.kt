package paprika.render

import paprika.render.pglObject.LwjglObject
import paprika.render.pglObject.PGLObject


val PGLObject.id: Int
    get() = (this as LwjglObject).id