package paprika.render.glObject

import paprika.render.pglObject.*

class IOSProgram(override val id: UInt) : IOSObject, PGLProgram
class IOSShader(override val id: UInt) : PGLShader, IOSObject
class IOSBuffer(override val id: UInt) : PGLBuffer, IOSObject
class IOSRenderBuffer(override val id: UInt) : PGLRenderbuffer, IOSObject
class IOSTexture(override val id: UInt) : PGLTexture, IOSObject
class IOSUniformLocation(val id: Int) : PGLUniformLocation
