package paprika.render

import paprika.render.glObject.IOSObject
import paprika.render.glObject.IOSUniformLocation
import paprika.render.pglObject.PGLObject
import paprika.render.pglObject.PGLUniformLocation

val PGLObject.id: UInt
    get() = (this as IOSObject).id

val PGLUniformLocation.id: Int
    get() = (this as IOSUniformLocation).id

