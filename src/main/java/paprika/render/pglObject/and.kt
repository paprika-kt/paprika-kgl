package paprika.render.pglObject

import paprika.render.pglObject.*


class AndProgram(override val id: Int) : AndObject, PGLProgram
class AndShader(override val id: Int) : PGLShader, AndObject
class AndBuffer(override val id: Int) : PGLBuffer, AndObject
class AndRenderBuffer(override val id: Int) : PGLRenderbuffer, AndObject
class AndTexture(override val id: Int) : PGLTexture, AndObject
class AndUniformLocation(override val id: Int) : PGLUniformLocation, AndObject
