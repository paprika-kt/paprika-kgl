@file:Suppress("UsePropertyAccessSyntax")

package paprika.render

import org.khronos.webgl.*
import paprika.io.buffer.ByteArrayBuffer
import paprika.io.buffer.FloatArrayBuffer
import paprika.io.buffer.IntArrayBuffer
import paprika.io.buffer.ShortArrayBuffer
import paprika.canvas.sources.ImageSource
import paprika.io.HtmlImageSource
import paprika.render.pglObject.*
import paprika.render.pglObject.WebGLFramebuffer
import paprika.render.pglObject.WebGLShaderPrecisionFormat

class KJSRendererContext(var gl: WebGLRenderingContext) : RenderingContext {


    override val drawingBufferWidth: Int
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val drawingBufferHeight: Int
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun getContextAttributes(): PGLContextAttributes? = TODO()

    override fun isContextLost(): Boolean = gl.isContextLost()

    override fun getSupportedExtensions(): Array<String> = gl.getSupportedExtensions() as Array<String>

    override fun getExtension(name: String): Any = gl.getExtension(name)

    override fun activeTexture(texture: Int) = gl.activeTexture(texture)

    override fun attachShader(program: PGLProgram, shader: PGLShader) = gl.attachShader(program.value, shader.value)

    override fun bindAttribLocation(program: PGLProgram, index: Int, name: String) = gl.bindAttribLocation(program.value, index, name)

    override fun bindBuffer(target: Int, buffer: PGLBuffer?) = gl.bindBuffer(target, buffer?.value)

    override fun bindFramebuffer(target: Int, framebuffer: WebGLFramebuffer) = TODO()

    override fun bindRenderbuffer(target: Int, renderbuffer: PGLRenderbuffer) = TODO()

    override fun bindTexture(target: Int, texture: PGLTexture) = gl.bindTexture(target, texture.value)

    override fun blendColor(red: Float, green: Float, blue: Float, alpha: Float) = gl.blendColor(red, green, blue, alpha)

    override fun blendEquation(mode: Int) = gl.blendEquation(mode)

    override fun blendEquationSeparate(modeRGB: Int, modeAlpha: Int) = gl.blendEquationSeparate(modeRGB, modeAlpha)

    override fun blendFunc(sfactor: Int, dfactor: Int) = gl.blendFunc(sfactor, dfactor)

    override fun blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int) = gl.blendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha)

    override fun bufferData(target: Int, size: Int, usage: Int) = gl.bufferData(target, size, usage)

    override fun bufferData(target: Int, data: PBufferDataSource?, usage: Int) = TODO()

    override fun bufferData(target: Int, data: FloatArrayBuffer, usage: Int) = gl.bufferData(target, data.buffer, usage)
    override fun bufferData(target: Int, data: ShortArrayBuffer, usage: Int) = gl.bufferData(target, data.buffer, usage)
    override fun bufferData(target: Int, data: IntArrayBuffer, usage: Int) = gl.bufferData(target, data.buffer, usage)
    override fun bufferData(target: Int, data: ByteArrayBuffer, usage: Int) = gl.bufferData(target, data.buffer, usage)

    override fun bufferSubData(target: Int, offset: Int, data: PBufferDataSource?) = TODO()

    override fun checkFramebufferStatus(target: Int): Int = gl.checkFramebufferStatus(target)

    override fun clear(mask: Int) = gl.clear(mask)

    override fun clearColor(red: Float, green: Float, blue: Float, alpha: Float) = gl.clearColor(red, green, blue, alpha)

    override fun clearDepth(depth: Float) = gl.clearDepth(depth)

    override fun clearStencil(s: Int) = gl.clearStencil(s)

    override fun colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean) = gl.colorMask(red, green, blue, alpha)

    override fun compileShader(shader: PGLShader) = gl.compileShader(shader.value)

    override fun compressedTexImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, data: PGLArrayBufferView) = TODO()

    override fun compressedTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, data: PGLArrayBufferView) = TODO()

    override fun copyTexImage2D(target: Int, level: Int, internalformat: Int, x: Int, y: Int, width: Int, height: Int, border: Int) = gl.copyTexImage2D(target, level, internalformat, x, y, width, height, border)

    override fun copyTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, x: Int, y: Int, width: Int, height: Int) = gl.copyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height)

    override fun createBuffer(): PGLBuffer = KJSBuffer(gl.createBuffer()!!)

    override fun createFramebuffer(): WebGLFramebuffer? = TODO()

    override fun createProgram(): PGLProgram? = KJSProgram(gl.createProgram()!!)

    override fun createRenderbuffer(): PGLRenderbuffer? = TODO()

    override fun createShader(type: Int): PGLShader = KJSShader(gl.createShader(type)!!)

    override fun createTexture(): PGLTexture = KJSTexture(gl.createTexture()!!)

    override fun cullFace(mode: Int) = gl.cullFace(mode)

    override fun deleteBuffer(buffer: PGLBuffer) = gl.deleteBuffer(buffer.value)

    override fun deleteFramebuffer(framebuffer: WebGLFramebuffer) = TODO()

    override fun deleteProgram(program: PGLProgram) = gl.deleteProgram(program.value)

    override fun deleteRenderbuffer(renderbuffer: PGLRenderbuffer) = TODO()

    override fun deleteShader(shader: PGLShader) = gl.deleteShader(shader.value)

    override fun deleteTexture(texture: PGLTexture) = gl.deleteTexture(texture.value)

    override fun depthFunc(func: Int) = gl.depthFunc(func)

    override fun depthMask(flag: Boolean) = gl.depthMask(flag)

    override fun depthRange(zNear: Float, zFar: Float) = gl.depthRange(zNear, zFar)

    override fun detachShader(program: PGLProgram, shader: PGLShader) = gl.detachShader(program.value, shader.value)

    override fun disable(cap: Int) = gl.disable(cap)

    override fun disableVertexAttribArray(index: Int) = gl.disableVertexAttribArray(index)

    override fun drawArrays(mode: Int, first: Int, count: Int) = gl.drawArrays(mode, first, count)

    override fun drawElements(mode: Int, count: Int, type: Int, offset: Int) = gl.drawElements(mode, count, type, offset)

    override fun enable(cap: Int) {
//        println(cap)
        gl.enable(cap)
    }

    override fun enableVertexAttribArray(index: Int) = gl.enableVertexAttribArray(index)

    override fun finish() = gl.finish()

    override fun flush() = gl.flush()

    override fun framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: PGLRenderbuffer) = TODO()

    override fun framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: PGLTexture, level: Int) = gl.framebufferTexture2D(target, attachment, textarget, texture.value, level)

    override fun frontFace(mode: Int) = gl.frontFace(mode)

    override fun generateMipmap(target: Int) = gl.generateMipmap(target)

    override fun getActiveAttrib(program: PGLProgram, index: Int): PGLActiveInfo? = TODO()

    override fun getActiveUniform(program: PGLProgram, index: Int): PGLActiveInfo? = TODO()

    override fun getAttachedShaders(program: PGLProgram): Array<PGLShader>? = TODO()

    override fun getAttribLocation(program: PGLProgram, name: String): Int = gl.getAttribLocation(program.value, name)

    override fun getBufferParameter(target: Int, pname: Int): Any? = gl.getBufferParameter(target, pname)

    override fun getParameter(pname: Int): Any? = gl.getParameter(pname)

    override fun getError(): Int = gl.getError()

    override fun getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): Any? = gl.getFramebufferAttachmentParameter(target, attachment, pname)

    override fun getProgramParameter(program: PGLProgram, pname: Int): Any? = gl.getProgramParameter(program.value, pname)

    override fun getProgramInfoLog(program: PGLProgram): String? = gl.getProgramInfoLog(program.value)

    override fun getRenderbufferParameter(target: Int, pname: Int): Any? = gl.getRenderbufferParameter(target, pname)

    override fun getShaderParameter(shader: PGLShader, pname: Int): Any? = gl.getShaderParameter(shader.value, pname)

    override fun getShaderPrecisionFormat(shadertype: Int, precisiontype: Int): WebGLShaderPrecisionFormat? = TODO()

    override fun getShaderInfoLog(shader: PGLShader): String? = gl.getShaderInfoLog(shader.value)

    override fun getShaderSource(shader: PGLShader): String? = gl.getShaderSource(shader.value)

    override fun getTexParameter(target: Int, pname: Int): Any? = gl.getTexParameter(target, pname)

    override fun getUniform(program: PGLProgram, location: PGLUniformLocation): Any? = TODO()

    override fun getUniformLocation(program: PGLProgram, name: String): PGLUniformLocation = KJSUniformLocation(gl.getUniformLocation(program.value, name)!!)

    override fun getVertexAttrib(index: Int, pname: Int): Any? = gl.getVertexAttrib(index, pname)

    override fun getVertexAttribOffset(index: Int, pname: Int): Int = gl.getVertexAttribOffset(index, pname)

    override fun hint(target: Int, mode: Int) = gl.hint(target, mode)

    override fun isBuffer(buffer: PGLBuffer): Boolean = gl.isBuffer(buffer.value)

    override fun isEnabled(cap: Int): Boolean = gl.isEnabled(cap)

    override fun isFramebuffer(framebuffer: WebGLFramebuffer?): Boolean = TODO()

    override fun isProgram(program: PGLProgram): Boolean = gl.isProgram(program.value)

    override fun isRenderbuffer(renderbuffer: PGLRenderbuffer): Boolean = TODO() //gl.isRenderbuffer()

    override fun isShader(shader: PGLShader): Boolean = gl.isShader(shader.value)

    override fun isTexture(texture: PGLTexture): Boolean = gl.isTexture(texture.value)

    override fun lineWidth(width: Float) = gl.lineWidth(width)

    override fun linkProgram(program: PGLProgram) = gl.linkProgram(program.value)

    override fun pixelStorei(pname: Int, param: Int) = gl.pixelStorei(pname, param)

    override fun polygonOffset(factor: Float, units: Float) = gl.polygonOffset(factor, units)

    override fun readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, type: Int, pixels: PGLArrayBufferView?) = TODO()

    override fun renderbufferStorage(target: Int, internalformat: Int, width: Int, height: Int) = gl.renderbufferStorage(target, internalformat, width, height)

    override fun sampleCoverage(value: Float, invert: Boolean) = gl.sampleCoverage(value, invert)

    override fun scissor(x: Int, y: Int, width: Int, height: Int) = gl.scissor(x, y, width, height)

    override fun shaderSource(shader: PGLShader, source: String) = gl.shaderSource(shader.value, source)

    override fun stencilFunc(func: Int, ref: Int, mask: Int) = gl.stencilFunc(func, ref, mask)

    override fun stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int) = gl.stencilFuncSeparate(face, func, ref, mask)

    override fun stencilMask(mask: Int) = gl.stencilMask(mask)

    override fun stencilMaskSeparate(face: Int, mask: Int) = gl.stencilMaskSeparate(face, mask)

    override fun stencilOp(fail: Int, zfail: Int, zpass: Int) = gl.stencilOp(fail, zfail, zpass)

    override fun stencilOpSeparate(face: Int, fail: Int, zfail: Int, zpass: Int) = gl.stencilOpSeparate(face, fail, zfail, zpass)

    override fun texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, type: Int, pixels: ByteArrayBuffer) = gl.texImage2D(target, level, internalformat, width, height, border, format, type, pixels)
//    override fun texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, type: Int, pixels: UInt8ArrayBuffer) = gl.texImage2D(target, level, internalformat, width, height, border, format, type, pixels.buffer)

    override fun texImage2D(target: Int, level: Int, internalformat: Int, format: Int, type: Int, source: ImageSource) =
        if (source is HtmlImageSource) gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, source.image)
        else gl.texImage2D(target, level, internalformat, source.width, source.height, 0, format, type, source.bytes)

    override fun texParameterf(target: Int, pname: Int, param: Float) = gl.texParameterf(target, pname, param)

    override fun texParameteri(target: Int, pname: Int, param: Int) = gl.texParameteri(target, pname, param)

    override fun texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, type: Int, pixels: PGLArrayBufferView?) = TODO()

    override fun texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, type: Int, source: ImageSource?) = TODO()

    override fun uniform1f(location: PGLUniformLocation, x: Float) = gl.uniform1f(location.value, x)

    override fun uniform1fv(location: PGLUniformLocation, v: FloatArray) = gl.uniform1fv(location.value, v.unsafeCast<Array<Float>>())

    override fun uniform1fv(location: PGLUniformLocation, v: Array<Float>) = gl.uniform1fv(location.value, v)

    override fun uniform1i(location: PGLUniformLocation, x: Int) = gl.uniform1i(location.value, x)

    override fun uniform1iv(location: PGLUniformLocation, v: IntArray) = gl.uniform1iv(location.value, v.unsafeCast<Array<Int>>())

    override fun uniform1iv(location: PGLUniformLocation, v: Array<Int>) = gl.uniform1iv(location.value, v)

    override fun uniform2f(location: PGLUniformLocation, x: Float, y: Float) = gl.uniform2f(location.value, x, y)

    override fun uniform2fv(location: PGLUniformLocation, v: FloatArray) = gl.uniform2fv(location.value, v.unsafeCast<Array<Float>>())

    override fun uniform2fv(location: PGLUniformLocation, v: Array<Float>) = gl.uniform2fv(location.value, v)

    override fun uniform2i(location: PGLUniformLocation, x: Int, y: Int) = gl.uniform2i(location.value, x, y)

    override fun uniform2iv(location: PGLUniformLocation, v: IntArray) = gl.uniform2iv(location.value, v.unsafeCast<Array<Int>>())

    override fun uniform2iv(location: PGLUniformLocation, v: Array<Int>) = gl.uniform2iv(location.value, v)

    override fun uniform3f(location: PGLUniformLocation, x: Float, y: Float, z: Float) = gl.uniform3f(location.value, x, y, z)

    override fun uniform3fv(location: PGLUniformLocation, v: FloatArray) = gl.uniform3fv(location.value, v.unsafeCast<Array<Float>>())

    override fun uniform3fv(location: PGLUniformLocation, v: Array<Float>) = gl.uniform3fv(location.value, v)

    override fun uniform3i(location: PGLUniformLocation, x: Int, y: Int, z: Int) = gl.uniform3i(location.value, x, y, z)

    override fun uniform3iv(location: PGLUniformLocation, v: IntArray) = gl.uniform3iv(location.value, v.unsafeCast<Array<Int>>())

    override fun uniform3iv(location: PGLUniformLocation, v: Array<Int>) = gl.uniform3iv(location.value, v)

    override fun uniform4f(location: PGLUniformLocation, x: Float, y: Float, z: Float, w: Float) = gl.uniform4f(location.value, x, y, z, w)

    override fun uniform4fv(location: PGLUniformLocation, v: FloatArray) = gl.uniform4fv(location.value, v.unsafeCast<Array<Float>>())

    override fun uniform4fv(location: PGLUniformLocation, v: Array<Float>) = gl.uniform4fv(location.value, v)

    override fun uniform4i(location: PGLUniformLocation, x: Int, y: Int, z: Int, w: Int) = gl.uniform4i(location.value, x, y, z, w)

    override fun uniform4iv(location: PGLUniformLocation, v: IntArray) = gl.uniform4iv(location.value, v.unsafeCast<Array<Int>>())

    override fun uniform4iv(location: PGLUniformLocation, v: Array<Int>) = gl.uniform4iv(location.value, v)

    override fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) = gl.uniformMatrix2fv(location.value, transpose, value.unsafeCast<Array<Float>>())

    override fun uniformMatrix2fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) = gl.uniformMatrix2fv(location.value, transpose, value)

    override fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) = gl.uniformMatrix3fv(location.value, transpose, value.unsafeCast<Array<Float>>())

    override fun uniformMatrix3fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) = gl.uniformMatrix3fv(location.value, transpose, value)

    override fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: FloatArray) = gl.uniformMatrix4fv(location.value, transpose, value.unsafeCast<Array<Float>>())

    override fun uniformMatrix4fv(location: PGLUniformLocation, transpose: Boolean, value: Array<Float>) = gl.uniformMatrix4fv(location.value, transpose, value)

    override fun useProgram(program: PGLProgram?) = gl.useProgram(program?.value)

    override fun validateProgram(program: PGLProgram) = gl.validateProgram(program.value)

    override fun vertexAttrib1f(index: Int, x: Float) = gl.vertexAttrib1f(index, x)

    override fun vertexAttrib1fv(index: Int, values: FloatArray) = gl.vertexAttrib1fv(index, values)

    override fun vertexAttrib2f(index: Int, x: Float, y: Float) = gl.vertexAttrib2f(index, x, y)

    override fun vertexAttrib2fv(index: Int, values: FloatArray) = gl.vertexAttrib2fv(index, values)

    override fun vertexAttrib3f(index: Int, x: Float, y: Float, z: Float) = gl.vertexAttrib3f(index, x, y, z)

    override fun vertexAttrib3fv(index: Int, values: FloatArray) = gl.vertexAttrib3fv(index, values)

    override fun vertexAttrib4f(index: Int, x: Float, y: Float, z: Float, w: Float) = gl.vertexAttrib4f(index, x, y, z, w)

    override fun vertexAttrib4fv(index: Int, values: FloatArray) = gl.vertexAttrib4fv(index, values)

    override fun vertexAttribPointer(index: Int, size: Int, type: Int, normalized: Boolean, stride: Int, offset: Int) = gl.vertexAttribPointer(index, size, type, normalized, stride, offset.toInt())

    override fun viewport(x: Int, y: Int, width: Int, height: Int) = gl.viewport(x, y, width, height)

}


fun FloatArray.toFloat32Array(): Float32Array {
    val result = Float32Array(size)
    this.forEachIndexed { ind, f ->
        result[ind] = f
    }
    return result
}


fun ShortArray.toInt16Array(): Int16Array {
    val result = Int16Array(size)
    this.forEachIndexed { ind, f ->
        result[ind] = f
    }
    return result
}

