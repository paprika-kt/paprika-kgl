import org.gradle.internal.os.OperatingSystem

plugins {
    id("com.android.library")
    kotlin("multiplatform")
}

val lwjglVersion = "3.2.1"

val lwjglNatives = when (OperatingSystem.current()) {
    OperatingSystem.WINDOWS -> "natives-windows"
    OperatingSystem.LINUX -> "natives-linux"
    OperatingSystem.MAC_OS -> "natives-macos"
    else -> error("OS not recognized")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        minSdkVersion(15)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
    }
}

version = "0.0.1"

repositories {
    jcenter()
    google()
    mavenCentral()
}


tasks.withType<Test> {
    useJUnitPlatform()
}



kotlin {
    jvm()
    js()
    iosX64("ios") {
        binaries {
            framework()
        }
    }

    android("android")

    iosArm64("iosDevice") {
        binaries {
            framework()
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(project(":paprika-io"))
            }
        }
        val commonTest by getting {
            dependencies {
            }
        }

        val jvmMain by getting {
            tasks.withType<Test> {
                useJUnitPlatform()
            }
            dependencies {
                implementation(kotlin("stdlib-jdk8"))


                implementation("org.lwjgl:lwjgl:$lwjglVersion")
//                implementation("org.lwjgl:lwjgl-assimp:$lwjglVersion")
//                implementation("org.lwjgl:lwjgl-glfw:$lwjglVersion")
//                implementation("org.lwjgl:lwjgl-openal:$lwjglVersion")
                implementation("org.lwjgl:lwjgl-opengl:$lwjglVersion")
                implementation("org.lwjgl:lwjgl-stb:$lwjglVersion")

                //runtimeOnly not work??
                implementation("org.lwjgl:lwjgl:$lwjglVersion:$lwjglNatives")
//                implementation("org.lwjgl:lwjgl-assimp:$lwjglVersion:$lwjglNatives")
//                implementation("org.lwjgl:lwjgl-glfw:$lwjglVersion:$lwjglNatives")
//                implementation("org.lwjgl:lwjgl-openal:$lwjglVersion:$lwjglNatives")
                implementation("org.lwjgl:lwjgl-opengl:$lwjglVersion:$lwjglNatives")
                implementation("org.lwjgl:lwjgl-stb:$lwjglVersion:$lwjglNatives")

            }
        }

        val jvmTest by getting {
            tasks.withType<Test> {
                useJUnitPlatform()
            }
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }

        val androidMain by getting {
            dependencies {
                implementation(kotlin("stdlib"))
            }
        }

        val androidTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
    }
}